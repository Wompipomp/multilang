﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    [TestClass()]
    public class AssetDatabaseUtilTests
    {
        [TestMethod()]
        public void CombinePath_CombineTwoPath_ReturnsCorrectPath()
        {
            var path1 = "Assets";
            var path2 = "Resources";
            var expected = path1 + "/" + path2;
            var actual = AssetDatabaseUtil.CombinePath(path1, path2);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CombinePath_FirstPathWithTrailingSlash_ReturnsCorrectPath()
        {
            var path1 = "Assets/";
            var path2 = "Resources";
            var expected = "Assets/Resources";
            var actual = AssetDatabaseUtil.CombinePath(path1, path2);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CombinePath_SecondPathStartsWithSlash_ReturnsCorrectPath()
        {
            var path1 = "Assets";
            var path2 = "/Resources";
            var expected = "Assets/Resources";
            var actual = AssetDatabaseUtil.CombinePath(path1, path2);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CombinePath_TrimAllSlashedBetweenPath_ReturnsCorrectPath()
        {
            var path1 = "Assets///";
            var path2 = "//Resources";
            var expected = "Assets/Resources";
            var actual = AssetDatabaseUtil.CombinePath(path1, path2);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CombinePath_OnlyTrimSlashedBetweenPath_ReturnsCorrectPath()
        {
            var path1 = "/Assets";
            var path2 = "Resources/";
            var expected = "/Assets/Resources/";
            var actual = AssetDatabaseUtil.CombinePath(path1, path2);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CombinePath_Path1IsNull_ReturnsCorrectPath()
        {
            string path1 = null;
            var path2 = "Resources/";
            var expected = "/Resources/";
            var actual = AssetDatabaseUtil.CombinePath(path1, path2);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// MakeRelativeAssetPath
        /// </summary>

        private const string ApplicationPath = @"C:/Unity/Project/Assets/";

        [TestMethod()]
        public void MakeRelativeAssetPath_PathIsSubfolderOfAssets_ReturnRelativePath()
        {
            AssetDatabaseUtil.ApplicationPath = ApplicationPath;
            var fullPath = ApplicationPath + "Resources/";
            var expected = "Assets/Resources/";
            var actual = AssetDatabaseUtil.MakeRelativeAssetPath(fullPath);
            Assert.AreEqual(expected, actual);
        }
    }
}