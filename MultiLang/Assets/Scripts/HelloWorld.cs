﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEngine.UI;
using Wompipomp.MultiLang;

public class HelloWorld : MonoBehaviour
{
    public Text ScreenText;
    public Image Image;
    public Text ButtonText;
	// Use this for initialization
	void Start ()
	{
	    LocalizeStrings();
        Language.OnLanguageChanged += Language_OnLanguageChanged;
	}

    void OnDestroy()
    {
        Language.OnLanguageChanged -= Language_OnLanguageChanged;
    }

    private void Language_OnLanguageChanged(SystemLanguage lang)
    {
        LocalizeStrings();
    }

    private void LocalizeStrings()
    {
        ScreenText.text = Main.Label1 + Main.Label2;
        Image.sprite = Main.Image1;
        ButtonText.text = Main.ButtonText;
    }

    public void ChangeScene()
    {
        Application.LoadLevel(1);
    }

    public void ChangeLanguage()
    {
        if(Language.CurrentLanguage == SystemLanguage.Unknown)
            Language.ChangeLanguage(SystemLanguage.German);
        else if(Language.CurrentLanguage == SystemLanguage.German)
            Language.ChangeLanguage(SystemLanguage.Spanish);
        else
        {
            Language.ChangeLanguage(SystemLanguage.Unknown);
        }
    }


	
}
