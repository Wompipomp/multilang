﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Wompipomp.MultiLang
{
    /// <summary>
    /// Contains the languages and handles the conversions
    /// </summary>
    public static class Language
    {
        public delegate void LanguageEventHandler(SystemLanguage lang);
        public static event LanguageEventHandler OnLanguageChanged;
        private static SystemLanguage currentLanguage;
        private static readonly Dictionary<SystemLanguage, string> LanguageTable = new Dictionary<SystemLanguage, string>
        {
            //{ SystemLanguage.Unknown, string.Empty },
            
            
            {SystemLanguage.Afrikaans, "af"},
            {SystemLanguage.Arabic, "ar"},
            {SystemLanguage.Belarusian, "be"},
            {SystemLanguage.Bulgarian, "bg"},
            {SystemLanguage.Chinese, "zh"},
            {SystemLanguage.Czech, "cs"},
            {SystemLanguage.Danish, "da"},
            
            {SystemLanguage.English, "en"},
            { SystemLanguage.Estonian, "et"},
            {SystemLanguage.Finnish, "fi"},
            {SystemLanguage.French, "fr"},
            {SystemLanguage.German, "de"},
            { SystemLanguage.Greek, "el"},
            
            { SystemLanguage.Hebrew, "he"},
            {SystemLanguage.Hungarian, "hu"},
            {SystemLanguage.Indonesian, "id"},
            {SystemLanguage.Italian, "it"},
            {SystemLanguage.Japanese, "ja"},
            {SystemLanguage.Korean, "ko"},
            {SystemLanguage.Norwegian, "no"},
            {SystemLanguage.Polish, "pl"},
            {SystemLanguage.Portuguese, "pt"},
            {SystemLanguage.Romanian, "ro"},
            {SystemLanguage.Russian, "ru"},
            {SystemLanguage.Slovak, "sk"},
            {SystemLanguage.Spanish, "es"},
            {SystemLanguage.Swedish, "sv"},
            {SystemLanguage.Thai, "th"},
            {SystemLanguage.Turkish, "tr"},
            {SystemLanguage.Ukrainian, "uk"},
            {SystemLanguage.Vietnamese, "vi"}
        };

        /// <summary>
        /// One-time init of MultiLang with the current system language.
        /// </summary>
        static Language()
        {
            currentLanguage = Application.systemLanguage;
        }

        /// <summary>
        /// Gets the current set language
        /// </summary>
        public static SystemLanguage CurrentLanguage
        {
            get { return currentLanguage; }
        }

        /// <summary>
        /// Initializes MultiLang with a given language
        /// </summary>
        /// <param name="lang">The language you want to chose</param>
        public static void Initialize(SystemLanguage lang)
        {
            currentLanguage = lang;
        }

        /// <summary>
        /// Initializes MultiLang with the current Unity system language
        /// </summary>
        public static void Initialize()
        {
            currentLanguage = Application.systemLanguage;
        }

        /// <summary>
        /// Changes the language on runtime. Fires event OnLanguageChanged afterwards
        /// </summary>
        /// <param name="lang">New Language you want to set</param>
        public static void ChangeLanguage(SystemLanguage lang)
        {
            currentLanguage = lang;
            if (OnLanguageChanged != null)
                OnLanguageChanged(lang);
        }

        /// <summary>
        /// Converts a given language string to a two letter iso language
        /// </summary>
        /// <param name="lang">The given language</param>
        /// <returns>The two letter iso language. Empty string if incorrect language</returns>
        public static string ToTwoLetterIsoLanguageName(string lang)
        {
            var systemLang = (SystemLanguage) Enum.Parse(typeof (SystemLanguage), lang);
            return LanguageTable.ContainsKey(systemLang) ? LanguageTable[systemLang] : string.Empty;
        }

        /// <summary>
        /// Converts the current set SystemLanguage to two letter iso language 
        /// </summary>
        /// <returns>Two letter iso language. Empty string if value is not present</returns>
        public static string ToTwoLetterIsoLanguageName()
        {
            return LanguageTable.ContainsKey(currentLanguage) ? LanguageTable[currentLanguage] : string.Empty;
        }

        /// <summary>
        /// Returns the language table as string array
        /// </summary>
        /// <returns>language table as string array</returns>
        public static string[] GetKeyStrings()
        {
            return LanguageTable.Keys.Select(i => i.ToString()).ToArray();
        }
    }
}
