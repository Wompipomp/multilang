﻿using System;
using UnityEngine;

namespace Wompipomp.MultiLang
{
    /// <summary>
    /// Class for Sprite serialization.
    /// </summary>
    [Serializable]
    public class ResourceDatabaseItemSprite
    {
        [SerializeField]
        private string _key;
        [SerializeField]
        private Sprite _value;

        public ResourceDatabaseItemSprite()
        {
        }

        public ResourceDatabaseItemSprite(string key, Sprite value)
        {
            _key = key;
            _value = value;
        }

        public string Key
        {
            get { return _key; }
            set { _key = value; }
        }

        public Sprite Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
