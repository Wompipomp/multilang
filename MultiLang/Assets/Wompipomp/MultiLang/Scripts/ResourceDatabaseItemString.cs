﻿using System;
using UnityEngine;
using System.Collections;

namespace Wompipomp.MultiLang
{
    /// <summary>
    /// Class for string serialization
    /// </summary>
    [Serializable]
    public class ResourceDatabaseItemString
    {
        [SerializeField]
        private string _key;
        [SerializeField]
        private string _value;

        public ResourceDatabaseItemString()
        {
        }

        public ResourceDatabaseItemString(string key, string value)
        {
            _key = key;
            _value = value;
        }

        public string Key
        {
            get { return _key; }
            set { _key = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
