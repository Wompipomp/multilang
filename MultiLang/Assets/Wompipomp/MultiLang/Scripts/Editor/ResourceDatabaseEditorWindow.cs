﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace Wompipomp.MultiLang.Editor
{
    public enum ResourceDatabaseType
    {
        Strings = 0,
        Sprites
    }

    /// <summary>
    /// Base resource editor window with common functions
    /// </summary>
    public partial class ResourceDatabaseEditorWindow : EditorWindow
    {
        private readonly float BUTTON_WIDTH = 60;
        private readonly float GRID_SPACING = 15;
        private string _assetPath;
        private ResourceDatabase _resourceDatabase;
        private Vector2 _scrollPositionSprites;
        private Vector2 _scrollPositionStrings;
        private int _tab;

        [MenuItem("Wompipomp/MultiLang/Resource editor")]
        private static void Init()
        {
            var window = GetWindow<ResourceDatabaseEditorWindow>();
            window.minSize = new Vector2(600, 300);
            window.position = new Rect(200, 200, 600, 300);
            window.title = "Resource editor";
            window.Show();
        }

        [MenuItem("Wompipomp/MultiLang/Resource editor", true)]
        private static bool ValidateResourceDatabase()
        {
            if (Selection.activeObject == null)
            {
                return false;
            }
            var tempPath = AssetDatabase.GetAssetPath(Selection.activeObject);
            return ResourceDatabaseUtil.IsResourceDatabaseAsset(tempPath);
        }

        private void OnEnable()
        {
            if (_assetPath == null)
            {
                _assetPath = AssetDatabase.GetAssetPath(Selection.instanceIDs[0]);
                _resourceDatabase =
                    AssetDatabase.LoadAssetAtPath(_assetPath, typeof (ResourceDatabase)) as ResourceDatabase;
            }
        }

        private void OnGUI()
        {
            EditorGUI.BeginChangeCheck();
            _tab = GUILayout.Toolbar(_tab, new[] {"Strings", "Sprites"});
            switch (_tab)
            {
                case 0:
                    DrawStringsTab();
                    break;
                case 1:
                    DrawSpritesTab();
                    break;
            }
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(_resourceDatabase, "Modify");
                EditorUtility.SetDirty(_resourceDatabase);
            }
        }

        private void SaveAndClose()
        {
            if (GUILayout.Button("Close", GUILayout.Width(150)))
            {
                EditorUtility.SetDirty(_resourceDatabase);
                Close();
            }
        }

        private void CreateResourceFile()
        {
            if (GUILayout.Button("Create resource class", GUILayout.Width(150)))
            {
                EditorUtility.SetDirty(_resourceDatabase);
                var prefix = GetCurrentFilePrefix();
                if (string.IsNullOrEmpty(prefix))
                {
                    Debug.LogError("Could not get filename from path: " + _assetPath);
                    return;
                }
                var resourceClassPath = GetResourceClassPath(prefix); 
                if (!AssetDatabase.IsValidFolder(Settings.FULL_SCRIPTS_PATH))
                    AssetDatabaseUtil.CreateFolder(Settings.FULL_SCRIPTS_PATH);
                var filledTemplate = ResourceClassCreator.FillResourceTemplate(_resourceDatabase, prefix);

                if (File.Exists(resourceClassPath))
                    File.Delete(resourceClassPath);
                File.WriteAllText(resourceClassPath, filledTemplate);
                Debug.Log("New resource class created in Scripts/MultiLang");
                AssetDatabase.Refresh();
            }
        }

        private void CopyKeys(ResourceDatabaseType type)
        {
            if (GUILayout.Button("Copy keys", GUILayout.Width(100)))
            {
                var path = AssetDatabaseUtil.OpenAssetFilePanel("Choose resource database to copy from",
                    "Assets/Resources",
                    "*.*");
                if (string.IsNullOrEmpty(path))
                {
                    return;
                }
                if (ResourceDatabaseUtil.IsResourceDatabaseAsset(path))
                {
                    var defaultDb =
                        AssetDatabase.LoadAssetAtPath(path, typeof (ResourceDatabase)) as ResourceDatabase;
                    if (defaultDb == null)
                    {
                        Debug.Log("Error loading database at path: " + path);
                        return;
                    }
                    switch (type)
                    {
                        case ResourceDatabaseType.Strings:
                            _resourceDatabase.Strings.AddRangeByValue(defaultDb.Strings);
                            break;
                        case ResourceDatabaseType.Sprites:
                            _resourceDatabase.Sprites.AddRangeByValue(defaultDb.Sprites);
                            break;
                    }
                  
                }
                else
                {
                    Debug.LogError("Asset is no resource database");
                }
            }
        }

        private static string GetResourceClassPath(string prefix)
        {
            return AssetDatabaseUtil.CombinePath(Settings.FULL_SCRIPTS_PATH, prefix + ".cs");
        }

        private string GetCurrentFilePrefix()
        {
            var fileName = Path.GetFileName(_assetPath);
            return !string.IsNullOrEmpty(fileName) ? fileName.Split('.')[0] : null;
        }

        private void FillHeader()
        {
            var width = position.width;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Name", GUILayout.Width(width/2 - BUTTON_WIDTH/2));
            EditorGUILayout.LabelField("Value", GUILayout.Width(width/2 - BUTTON_WIDTH/2));
            EditorGUILayout.EndHorizontal();
        }
    }
}