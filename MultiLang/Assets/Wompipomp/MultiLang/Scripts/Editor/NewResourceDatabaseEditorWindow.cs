﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine.UI;

namespace Wompipomp.MultiLang.Editor
{
    /// <summary>
    /// Helper to create a new database resource.
    /// </summary>
    public class NewResourceDatabaseEditorWindow : EditorWindow
    {

        private string _filename;
        private string _prefix;
        private int _selectedLanguageItem = 0;
        private readonly string[] _langs = Language.GetKeyStrings();
        private int _selectedResourceDatabaseItem;
        private KeyValuePair<string,int> _defaultLanguageListItem = new KeyValuePair<string, int>("Default", 0); 
        private KeyValuePair<string,int> _defaultResourceDatabaseListItem = new KeyValuePair<string, int>("None", 0);

        private string[] _allResourceDatabaseAssets;

       

    [MenuItem("Wompipomp/MultiLang/Create new resource database")]
        static void Init()
        {
            var window = EditorWindow.GetWindow<NewResourceDatabaseEditorWindow>();
            window.title = "New resource";
        window.position = new Rect(200, 200, 400, 200);
            window.Show();

        }

        void OnEnable()
        {
            _allResourceDatabaseAssets = ResourceDatabaseUtil.GetAllResourceDatabaseAssets();
        }
         
        private void OnGUI()
        {
            PrefixTextField();
            LanguageDropDownList();
            CopyFromTemplateDropDownList();
            EditorGUILayout.LabelField("Filename: " + _filename);
            Buttons();
        }

        private void Buttons()
        {
            if (GUILayout.Button("Create"))
            {
                var cancel = CheckInput();
                if (cancel)
                    return;
                CreateResource();
            }
        }

        private void CopyFromTemplateDropDownList()
        {
            _selectedResourceDatabaseItem = EditorGUILayout.Popup("Copy from:", _selectedResourceDatabaseItem,
                new[] {_defaultResourceDatabaseListItem.Key}.Concat(_allResourceDatabaseAssets).ToArray());
        }

        private void LanguageDropDownList()
        {
            _selectedLanguageItem = EditorGUILayout.Popup("Language", _selectedLanguageItem,
                new[] {_defaultLanguageListItem.Key}.Concat(_langs).ToArray());
            if (_selectedLanguageItem == _defaultLanguageListItem.Value)
                _filename = _prefix + ".asset";
            else
                _filename = _prefix + "." + Language.ToTwoLetterIsoLanguageName(_langs[_selectedLanguageItem - 1]) + ".asset";
        }

        private void PrefixTextField()
        {
            _prefix = EditorGUILayout.TextField("Prefix", _prefix);
        }

        private bool CheckInput()
        {
            if (string.IsNullOrEmpty(_prefix))
            {
                Debug.LogError("Please enter a valid filename");
                return true;
            }

            if (_prefix.Contains('.'))
            {
                Debug.LogError("Filename must not contain .");
                return true;
            }
            if (_filename.IndexOfAny(Path.GetInvalidFileNameChars()) >= 0)
            {
                Debug.LogError("Filename contains invalid chars");
                return true;
            }
           
            return false;

        }

        private void CreateResource()
        {
            if (IsAssetWithSameNameInProject())
            {
                Debug.LogError("There is already an asset with the same name in the project");
                return;
            }
             
            if (!AssetDatabase.IsValidFolder(Settings.FULL_RESOURCES_PATH))
                AssetDatabaseUtil.CreateFolder(Settings.FULL_RESOURCES_PATH);

            var db = ScriptableObject.CreateInstance<ResourceDatabase>();
            AssetDatabase.CreateAsset(db, AssetDatabaseUtil.CombinePath(Settings.FULL_RESOURCES_PATH, _filename));
            if (_selectedResourceDatabaseItem > _defaultResourceDatabaseListItem.Value)
            {
                var templateResourceDatabase =
                    AssetDatabase.LoadAssetAtPath(_allResourceDatabaseAssets[_selectedResourceDatabaseItem - 1], typeof(ResourceDatabase)) as ResourceDatabase;
                if (templateResourceDatabase != null)
                {
                    db.Strings.AddRangeByValue(templateResourceDatabase.Strings);
                    db.Sprites.AddRangeByValue(templateResourceDatabase.Sprites);
                }
            }
            Debug.Log("New resource database created in Assets/Resources");
            AssetDatabase.SaveAssets();
        }

        private bool IsAssetWithSameNameInProject()
        {
            var paths = ResourceDatabaseUtil.GetAllResourceDatabaseAssets();
            foreach (var assetPath in paths)
            {
                var filename = Path.GetFileName(assetPath);
                if (string.IsNullOrEmpty(filename))
                    continue;
                if (_filename.ToLower() == filename.ToLower())
                {
                    return true;
                }
            }
            return false;
        }
    }
}
