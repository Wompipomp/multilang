﻿using UnityEngine;
using System.Collections;

namespace Wompipomp.MultiLang.Editor
{
    /// <summary>
    /// Common paths and settings
    /// </summary>
    public static class Settings
    {
        //Path where resource class will be saved
        public const string FULL_SCRIPTS_PATH = "Assets/Scripts/MultiLang";
        //Path where new resource databases will be saved
        public const string FULL_RESOURCES_PATH = "Assets/Resources";

        //Namespace of the generated resource class
        public const string RESOURCE_CLASS_NAMESPACE = "Wompipomp.MultiLang";

    }
}