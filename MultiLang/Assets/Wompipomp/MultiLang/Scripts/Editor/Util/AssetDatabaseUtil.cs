﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public static class AssetDatabaseUtil
{
    public static string ApplicationPath;

    public static void CreateFolder(string path)
    {
        var currentPath = string.Empty;
        path = path.TrimEnd('/');
        var folders = path.Split('/');
        foreach (var token in folders)
        {
            var tempPath = CombinePath(currentPath, token);
            if (!AssetDatabase.IsValidFolder(tempPath))
            AssetDatabase.CreateFolder(currentPath, token);
            currentPath = tempPath;
        }
    }

    public static string CombinePath(string path1, string path2)
    {
        if (string.IsNullOrEmpty(path1) && string.IsNullOrEmpty(path2))
            return string.Empty;
        if (string.IsNullOrEmpty(path1))
            return path2.TrimStart('/');
        if (string.IsNullOrEmpty(path2))
            return path1.TrimEnd('/');
        path1 = path1.TrimEnd('/');
        path2 = path2.TrimStart('/');
        return path1 + "/" + path2;
    }

    public static string OpenAssetFilePanel(string title, string folder, string filter)
    {
        var fullPath = EditorUtility.OpenFilePanel(title, folder, filter);
        return MakeRelativeAssetPath(fullPath);
    }

    public static string MakeRelativeAssetPath(string fullPath)
    {
        if (fullPath.ToLower().StartsWith(Application.dataPath.ToLower()))
        {
            return CombinePath("Assets", fullPath.Substring(Application.dataPath.Length));
        }
        return null;
    }
}
