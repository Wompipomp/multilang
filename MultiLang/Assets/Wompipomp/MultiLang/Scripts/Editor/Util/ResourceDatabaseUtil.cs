﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using Wompipomp.MultiLang;

public static class ResourceDatabaseUtil {
    public static string[] GetAllResourceDatabaseAssets()
    {
        var allAssets = AssetDatabase.GetAllAssetPaths();
        return allAssets.Where(i => i.ToLower().EndsWith(".asset") && IsResourceDatabaseAsset(i)).ToArray();
    }

    public static bool IsResourceDatabaseAsset(string filename)
    {
        var db = AssetDatabase.LoadAssetAtPath(filename, typeof(ResourceDatabase)) as ResourceDatabase;
        return db != null;
    }
}
