﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using Wompipomp.MultiLang;

public static class Extension {
    public static void AddRangeByValue(this List<ResourceDatabaseItemString> list,
        List<ResourceDatabaseItemString> toCopy)
    {
        foreach (var item in toCopy)
        {
            if (list.Find(i => i.Key == item.Key) == null)
            {
                list.Add(new ResourceDatabaseItemString(item.Key, item.Value));
            }
        }
    }

    public static void AddRangeByValue(this List<ResourceDatabaseItemSprite> list,
        List<ResourceDatabaseItemSprite> toCopy)
    {
        foreach (var item in toCopy)
        {
            if (list.Find(i => i.Key == item.Key) == null)
            {
                list.Add(new ResourceDatabaseItemSprite(item.Key, item.Value));
            }
        }
    }
}
