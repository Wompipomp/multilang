﻿using System.IO;
using System.Text;
using Wompipomp.MultiLang;

namespace Wompipomp.MultiLang.Editor
{
    /// <summary>
    /// Helper to create the resource classes for the assets.
    /// Modify this class if you want to change the code genereated for the resource classes
    /// </summary>
    public static class ResourceClassCreator
    {
        
        private const string _methodTemplateStrings =
@"
        public static string {0}
        {{
            get
            {{
                if (db == null || cachedLanguage != Language.ToTwoLetterIsoLanguageName()) InitResource();
                if (db != null)
                {{
                    var text = db.Strings.Find(i => i.Key == ""{0}"");
                    if (text != null)
                        return text.Value;
                }}
                var defaultText = defaultDb.Strings.Find(i => i.Key == ""{0}"");
                return defaultText != null ? defaultText.Value : string.Empty;
            }}
        }}

";

        private const string _methodTemplateSprites =
@"
        public static Sprite {0}
        {{
            get
            {{
                if (db == null || cachedLanguage != Language.ToTwoLetterIsoLanguageName()) InitResource();
                if (db != null)
                {{
                    var text = db.Sprites.Find(i => i.Key == ""{0}"");
                    if (text != null)
                        return text.Value;
                }}
                var defaultText = defaultDb.Sprites.Find(i => i.Key == ""{0}"");
                return defaultText != null ? defaultText.Value : null;
            }}
        }}

";

        private const string _fileTemplate =
            @"
using UnityEngine;
using Wompipomp.MultiLang;

/// This file is autogenerated. All changes will be overriden.
namespace {2}
{{
    public static class {0}
    {{
        private static ResourceDatabase db;
        private static ResourceDatabase defaultDb;
        private static string cachedLanguage;

        public static void InitResource()
        {{
            cachedLanguage = Language.ToTwoLetterIsoLanguageName();
            if (cachedLanguage != string.Empty)
                db = Resources.Load<ResourceDatabase>(""{0}."" + Language.ToTwoLetterIsoLanguageName());
            else
                db = Resources.Load<ResourceDatabase>(""{0}"");
            defaultDb = Resources.Load<ResourceDatabase>(""{0}"");
 
            if (db == null && defaultDb == null)
            {{
                Debug.LogError(""Can not find resource asset database"");
            }}
        }}

        {1}
    }}

}}";

        /// <summary>
        /// Generates the code from templates above and given values
        /// </summary>
        /// <param name="db">Resource database to generate resource class for</param>
        /// <param name="prefix">Name for the resource class and the resource database.</param>
        /// <returns>Generated code template</returns>
        public static string FillResourceTemplate(ResourceDatabase db, string prefix)
        {
            //Generate code for string getter
            StringBuilder methods = new StringBuilder();
            for (int i = 0; i < db.Strings.Count; i++)
            {
                ResourceDatabaseItemString itemString = db.Strings[i];
                var format = string.Format(_methodTemplateStrings, itemString.Key);
                methods.Append(format);
            }

            //Generate code for sprite getter
            for (int i = 0; i < db.Sprites.Count; i++)
            {
                ResourceDatabaseItemSprite itemSprite = db.Sprites[i];
                var format = string.Format(_methodTemplateSprites, itemSprite.Key);
                methods.Append(format);
            }

            return string.Format(_fileTemplate, prefix, methods, Settings.RESOURCE_CLASS_NAMESPACE);
            
        }
    }
}