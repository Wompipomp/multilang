﻿using UnityEngine;
using UnityEditor;

namespace Wompipomp.MultiLang.Editor
{
    /// <summary>
    /// Partial class for sprites tab
    /// </summary>
    public partial class ResourceDatabaseEditorWindow
    {
        private void DrawSpritesTab()
        {
            _scrollPositionSprites = EditorGUILayout.BeginScrollView(_scrollPositionSprites);
            FillHeader();
            ListViewSprites();
            ButtonsSprites();
            EditorGUILayout.EndScrollView();
        }

        private void ListViewSprites()
        {
            var width = position.width;

            for (var i = 0; i < _resourceDatabase.Sprites.Count; i++)
            {
                var item = _resourceDatabase.Sprites[i];

                EditorGUILayout.BeginHorizontal();
                item.Key = EditorGUILayout.TextField(item.Key, GUILayout.Width(width / 2 - BUTTON_WIDTH / 2));
                item.Value = (Sprite)EditorGUILayout.ObjectField(item.Value, typeof(Sprite), true, GUILayout.Width(width / 2 - BUTTON_WIDTH / 2));
                _resourceDatabase.Sprites[i] = item;
                GUI.skin.button.margin.top = 0;
                RemoveButton(i, ResourceDatabaseType.Sprites);
                EditorGUILayout.EndHorizontal();
            }
        }

        private void ButtonsSprites()
        {
            EditorGUILayout.BeginHorizontal();
            AddItemSprites();
            CopyKeys(ResourceDatabaseType.Sprites);
            GUILayout.FlexibleSpace();
            CreateResourceFile();
            SaveAndClose();
            EditorGUILayout.EndHorizontal();
        }

        private void AddItemSprites()
        {
            if (GUILayout.Button("+", GUILayout.Width(50)))
                _resourceDatabase.Sprites.Add(new ResourceDatabaseItemSprite());
        }

    }
}
