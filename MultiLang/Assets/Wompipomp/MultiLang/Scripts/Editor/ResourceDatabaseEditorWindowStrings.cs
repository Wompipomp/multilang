﻿using UnityEngine;
using UnityEditor;

namespace Wompipomp.MultiLang.Editor
{
    /// <summary>
    /// Partial class for strings tab
    /// </summary>
    public partial class ResourceDatabaseEditorWindow
    {

        private void DrawStringsTab()
        {
            _scrollPositionStrings = EditorGUILayout.BeginScrollView(_scrollPositionStrings);
            FillHeader();
            ListViewStrings();
            ButtonsString();
            EditorGUILayout.EndScrollView();
        }

        private void ListViewStrings()
        {
            var width = position.width;

            for (var i = 0; i < _resourceDatabase.Strings.Count; i++)
            {
                var item = _resourceDatabase.Strings[i];

                EditorGUILayout.BeginHorizontal();
                item.Key = EditorGUILayout.TextField(item.Key, GUILayout.Width(width / 2 - BUTTON_WIDTH / 2));
                item.Value = EditorGUILayout.TextArea(item.Value, GUILayout.Width(width / 2 - BUTTON_WIDTH / 2));
                GUI.skin.button.margin.top = 0;
                RemoveButton(i, ResourceDatabaseType.Strings);
                EditorGUILayout.EndHorizontal();
            }
        }

        private void RemoveButton(int i, ResourceDatabaseType type)
        {
            if (GUILayout.Button("-", GUILayout.Width(BUTTON_WIDTH - GRID_SPACING)))
            {
                switch (type)
                {
                    case ResourceDatabaseType.Strings:
                        _resourceDatabase.Strings.RemoveAt(i);
                        break;
                    case ResourceDatabaseType.Sprites:
                        _resourceDatabase.Sprites.RemoveAt(i);
                        break;
                }
                
            }
        }

        private void ButtonsString()
        {
            EditorGUILayout.BeginHorizontal();
            AddItemStrings();
            CopyKeys(ResourceDatabaseType.Strings);
            GUILayout.FlexibleSpace();
            CreateResourceFile();
            SaveAndClose();
            EditorGUILayout.EndHorizontal();
        }

        private void AddItemStrings()
        {
            if (GUILayout.Button("+", GUILayout.Width(50)))
                _resourceDatabase.Strings.Add(new ResourceDatabaseItemString());
        }

    }
}
