﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Wompipomp.MultiLang
{
    /// <summary>
    /// The database for saving the resources. If you want to add a different items to the database create a new
    /// ResourceDatabaseItemX class and add a List here.
    /// </summary>
    [Serializable]
    public class ResourceDatabase : ScriptableObject
    {
        [HideInInspector]
        public List<ResourceDatabaseItemString> Strings = new List<ResourceDatabaseItemString>();
        [HideInInspector]
        public List<ResourceDatabaseItemSprite> Sprites = new List<ResourceDatabaseItemSprite>(); 
    
    }
}
