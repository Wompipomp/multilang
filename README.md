﻿# MultiLang #
Asset for easy .NET resx-like resource localization in Unity3d

# Requirements #
Unity 5 or greater

Tested on Windows and Android

# Key features of MultiLang: #

- Uses ScriptableObject to store the data
- Uses Application.systemLanguage to determine the current language
- Possible use of many ScriptableObject databases for grouping and for different languages.
- Static getters to easily access localized data and for IntelliSense support
- EditorWindow to manage the localized data
- Localize strings and sprites.

# How-to in 4 steps: #
1. Create a default and one or more localized resource databases with the menu "Create resource database". Similar resource databases should have same prefix and differ in the language suffix like the resx files. The default database has no language suffix.
2. Put your content into the resource databases with the menu "Resource editor". Put all strings also into default resource database as this is the fallback if an entry isn't found.
3. Open the default resource database in resource editor and click "Create resource class". This creates a resource class to access the database. 
4. Access the data with <Prefix>.<Name>

# Setting up a specific language #
Usually MultiLang determines the current language with Application.systemLanguage, but you can also set a specific language if you like.
Just call Language.Initialize(SystemLanguage lang) to init MultiLang with a specific language.

# Change languages dynamically #
You can change a language dynamically with Language.ChangeLanguage(SystemLanguage). For the new language resources to take effect you have to set the strings / sprites anew. For this you can use the event Language.OnLanguageChanged which gets fired after the language was changed.


The source also contains a complete example.

# Credits #
Flags used in the example
The Ultimate Free Web Designer’s Icon Set (750 icons, 48x48px)
Designed by Oliver Twardowski (http://www.addictedtocoffee.de, @mywayhome on Twitter)

http://www.smashingmagazine.com/2010/04/15/the-ultimate-free-web-designer-s-icon-set-750-icons-incl-psd-sources/